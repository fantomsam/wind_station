\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[letterpaper, margin=1in]{geometry}
\usepackage{enumitem}
\usepackage{xfrac}
\usepackage{lmodern}
\usepackage{amsmath,amssymb}
\usepackage[]{pdfpages}
\usepackage{multicol}
\usepackage{gensymb}
\usepackage{textcomp}
\usepackage{caption}
\usepackage{graphicx}
\usepackage[colorlinks=true,linkcolor=black]{hyperref}
\usepackage{varwidth}
\usepackage{listings}
\usepackage[bottom]{footmisc}
\usepackage{fancyhdr}

\pagestyle{fancy}
\fancyhf{}
\lhead{Wind station Datalogger}
\chead{Technical Manual}
\rhead{Page \thepage}
\cfoot{\thepage}


%Commands
%text overline command
\newcommand{\textoverline}[1]{$\overline{\mbox{#1}}$}
%symbols declaration
\DeclareUnicodeCharacter{2127}{\mho}

%Title declaration
\title{Wind station Datalogger \\Technical Manual}
\date{May 18, 12019}
\author{Sam Harry Tzavaras}

\begin{document}
%Title page
\clearpage
\begin{figure}
\centering
  \includegraphics[width=5in]{../Artwork/Rview.png}
\end{figure}
\maketitle
\thispagestyle{empty}
%Licence page
\newpage
\section{License}
Copyright (C)  12019  Sam Harry Tzavaras.\\
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
 A copy of the license is included as a separate file with name "fdl-1.3.pdf".
\newpage
%Contents page
\newpage
\tableofcontents
\newpage

\section{Introduction}
This document is the technical manual for the Wind station datalogger.
For simplicity in the continue of this document the Wind station data-logger will be referred as device.
 \par
The purpose of this device is to acquire raw measurements and process them to wind characteristics.
The raw measurements acquire from two specific type sensors.
The device's implementation allowing connection with a wind speed tachometer,
and a wind direction transmitter with $2.5\degree$ resolution and parallel output coded in gray.
The processed measurements can be drive in to two analog indicators and be logged using NMEA0183 protocol over RS-232 serial line.
The analog outputs where provided are: a current output for a wind speed galvanometer,
and a 3phase voltage output for a desynn (dc-synchromotor) where used as wind direction indicator.

\section{Device's specifications}
\begin{table}[h!]
  \begin{center}
    \begin{tabular}{r||l}
      \textbf{Spec} & \textbf{Value} \\
      	\hline
      	Power Vin & 9Vrms or 12Vdc \\
      	Iin (@12Vdc)&40mA (Relay ON), 15mA (relay OFF) \\
	Initial outputs states & 0 mA (mAOut), 4V,1V,1V(Ph1..3) [0\degree], Relays OFF \\
	Relays Max Current & 500 mArms\\
	\hline
	Speed input frequency range&1..1252 Hz\\
	Direction bus values range&Symmetrical Gray code with 144 values (2.5\degree resolution)\\
	\hline
	Speed input counter resolution &  2.89uS\\
	DAC resolution &12bit\\
	DAC reference voltage& 2.5V\\
	\hline
	mA Output max & 5.844 mA\\
	mA Output min & $\approx$1.5 $\mu$A\\
	mA Output step & 1.43 $\mu$A\\
	\hline
	Synchro Output pole max ($V_{p-p}$) & 4V\\
	Synchro Output phase max ($V_{p-p}$)& 2.31V\\
	Synchro Output DC offset (ref to GND) & 2V\\
	Synchro Output phase diff& 120\degree\\
	\hline
      	Analog out Update rate & 800 SPS \\
      	NMEA Update rate & 1 SPS\\
	\hline
	RS232 baudrate & 9600 bps\\
	RS232 Start bit & 1 bit\\
	RS232 Stop bit & 1 bit\\
	RS232 handshake & RX off(Normal mode), XON/XOFF(Configure mode)\\
    \end{tabular}
   \caption{Device Specifications}
  \end{center}
\end{table}
\newpage
\enlargethispage{\baselineskip}
\section{Device's I/O Port Description}
\label{sec:desc}
\begin{figure}[h!]
	\includegraphics[width=\linewidth]{../Artwork/desc.png}
	\caption{Device Port Description}
	\label{fig:desc}
\end{figure}
\begin{center}
Description of Port names on figure \ref{fig:desc}
\end{center}
\begin{enumerate}[label=\Alph*.]
	\item Power connector (9Vrms or 12Vdc Polarity agnostic).
	\item Wind speed sensor supply and frequency input.
	\item Direction 8bit Bus and sensors supply
	\item Configuration mode input and ICSP port.
	\item Error (Direction sensor code) \&  Command Execution LED.
	\item RS-232 serial bus.
	\item SPDT Relay contacts.
	\item SPST Relay contacts.
	\item Wind speed indicators 3phase output (Desynn).
	\item Wind speed indicators (Current output)
\end{enumerate}

\newpage
\section{Principle of operation}
\subsection{Block Diagram}
\begin{figure}[h!]
	\centering
	\includegraphics[width=\linewidth,angle=0]{../Artwork/block_diag.png}
	\caption{Device Block diagram}
	\label{fig:block}
\end{figure}
The device's block diagram derived on figure \ref{fig:block}. The heart of the device is a $\mu$C (micro-controller).
The device's firmware is burned on the $\mu$C's program memory. The firmware is responsible for all the functionality that the device can provide.\\
The Firmware's functions in details:
\begin{enumerate}[]
	\item Measuring the speed frequency and scale it to wind speed with units m/s.
	\item Sampling the direction data from the 8bit shift register and scale it to degrees(\degree).
	\item Scale the wind speed value to a 12bit word for the current output.
	\item Scale the Direction value to three 12bit words for the DC synchro motor (desynn) outputs.
	\item Write the DAC over I2C bus and synchronize the DAC's outputs.
	\item Switch the wind speed instrument's range by change the calculations parameters
	\item Drive the relay outputs.
	\item Produce and send NMEA0183 Telegrams over RS-232 serial bus.
	\item Provide a Configuration interface mode.
\end{enumerate}
\par
The firmware is written in ANSI C and published as free software under GPLv3 or later license.
The firmware's executable made using the SDCC (compiler) and GPUTILS toolkit.
\par
The calculation in the firmware done in a fixed point way.
Due to this the calculation's arguments that required for the conversion of frequency to wind speed and wind speed to mA given in a fraction form.\\
A set of parameter with numerator and denominator is provided for the calibration of the indicator's scales and the frequency to m/s conversion.
\par
The device's $\mu$C base the clock source to an external high speed crystal.
The resonance frequency of the crystal is $11.0592Mhz$.
This value has been selected to give an accurate RS-232 baudrate. \par
For further information, the datasheet of the $\mu$C provided in the project's repository and the latest version of the source code.
Also, Datasheets for each integrated circuit and component that has been used,
together with references for all the used protocols,
can be found also in the project's repository.

\subsection{Power Supply Input}
The power supply of the device can handle input of DC(12Vdc) or AC(9Vrms).
It's based on the TR10S05 where is an integrated DC/DC converter,
this provide a 5Vdc rail which powers all the sub-circuits and the sensors. \par
**The Power supply input is polarity agnostic.

\subsection{Wind Speed Input}
The speed input is made to interface with a tachometer type Wind speed sensor.
The input is double buffered, first by a common emitter inverting amplifier that restores the frequency pulse train to TTL level,
and then followed from a schmitt trigger buffers which protect from slow slew rate edges.\\
The buffered pulse train driven to the internal Timer1 where is a module configured for measure the pulse train's period.
The conversion to frequency and the scaling to base units done in software in the main system loop.
The period measurement has 2.89uS${/bit}$ resolution.
\par
%**The software limits the measurement frequency to a range of 1 to 1252 Hz.

\subsection{Wind Direction input Bus \& Error/Execution LED}
This input interface with a wind direction sensor that have 8bit parallel output.
Each input is equip with a schmitt trigger buffers.
The transferring of input's bits to the $\mu$C done by a parallel to serial shift register (due to lack of $\mu$C pins).
The $\mu$C strobe the $Load$ line to sample the Direction inputs,
 and then receive each bit by pulse the $sCLK$ signal 7 times and the data readed thru the $Din$ line.
The form of the 8bit word is ONLY compatible with symmetric gray coded of 144 value. The Inputs bit weight order is marked on the PCB. \par
The Error/Execution LED on normal mode indicates (by lighting) if an illegal (out of scope) combination appears on the 8bit Wind direction bus.
At Configuration mode the Error/Execution LED indicate the execution of a command.

\subsection{System's DAC}
The device is equip with a 12bit 4 synchronous outputs DAC.
The communication between the $\mu$C and the DAC done by I$^2$C network.
The $\mu$C load the corresponding value(s) for mA \&  syncho phases (depend in which part of the program is executed) to the DAC's buffer registers,
and synchronize the conversion to analog by strobing the \textoverline{Sync} line.
The swing range of the DAC is selected to be 2.5V and the power on reset (POR) value $\approx0V$.
\newpage
\subsection{Relay Outputs}
The device is equip with 2 relays, a SPDT and a SPST (section \ref{sec:desc}). Both relays energized simultaneously from the $\mu$C thru a driver.
The functionality of each relay described bellow.
\begin{itemize}
	\item Speed range indicator (Port H) : A SPST contactor. Is OPEN in low range and CLOSE in high range
	\item  Speed range Switch (Port G) : A SPDT contactor. The contact COM-NC is CLOSE in Low range and the COM-NO in high Range.
\end{itemize}
The actuation signal for the Relays controlled by the speed range changing mechanism (implemented in software).
This mechanism can be described as a comparator, which compere the calculated wind speed with two thresholds.
The range changing mechanism is also equip with a hysteresis mechanism that preventing from any ``on threshold'' oscillation.
If the wind speed signal is grater than the ``low to high'' scale threshold the relays is activated,
and deactivated if the wind falls below of the high to low threshold value,
at the wind speed values between the two thresholds the state of the output hold the last value.
\par
A usage for the Speed range Switch (Port G) is to control the shunting resistor of the speed sensor indicator (galvanometer),
 with purpose to change the indication's range.
The Speed range indicator (Port H) contactor can be used as serial switching element to control some input from an another device,
 or a light or a small load $(\leq 500mA)$
\par
**The defaults threshold parameters for {\bf low to high} is 11.5m/s and for {\bf high to low} 10m/s.

\subsection{mA Output}
This output is designed to drive a galvanometer type wind speed indicator.
The output provide a current scaled accordingly to the state of the range selection mechanism, and proportional to the wind speed.
The current of the output is the product of the DAC's 4th analog Output (OUTD) passed thru a transconductance amplifier with scaler $2.34 m\mho$.\par
**The initial output value on boot is 0mA.
\par
**The default scaler for the {\bf Low} range is set for $89\mu A/_{m/s}$
\par
**The default scaler for the {\bf High} range is set for $95\mu A/_{m/s}$

\subsection{Synchro Output}
This output is designed to drive a wind direction indicator DC synchro motor (Desynn) type.\par
This output consist of 3 analog voltage outputs driven from the DAC's Outputs 1-3 (OUTA..C) passed thru three amplifiers with gain 1.6 $\sfrac{V}{V}$ (4.0824 dB).
The DAC's input of this 3 outputs (OUTA..C) produced by an angle to 3 phase cosine converter, implemented in software.
The input of this converter is the calculated angle from the wind Direction 8bit input bus.
In addition to that, a correction offset angle can be programmed (using the Device's Configuration interface),
as compensation for any mechanical error that the Desynn can have. \par
**The calculation of the cosine done in software using a lookup table.\par
**The initial value on boot for the Direction output is $0\degree$ \par
**The voltage (GND referenced) for each Synchro output pin have swing of $4V_{\text{p-p}}+2V_{dc}$

\subsection{RS-232 Port}
The device is equip with a RS-323 interface port. This port have two purposes,
to transmits of NMEA0183 telegrams (normal mode), and as user's terminal interface at the configurations mode.\par
The $\mu$C's UART controller drive the RS-232 Port thru a pair of level shifters.
The UART controller is capable for full duplex communication, but the reception used only at configuration mode.
The RS-232 Port's physical setup use baudrate of 9600BPS, 1 start/stop bit and software handshaking (XON/XOFF) to prevent any exceptions on execution. \par
**A crossover cable (null modem) is needed for connection with the device's RS-232 port.

\subsection{NMEA0183 Telegram}
The implementation of NMEA0183 in the firmware consist only from WIMWV telegram in talker mode. \\
The form of a WIMWV telegram is the following:
{\center \$WIMWV,SSS.S,T,DDD.D,M*CC $<$CR$>$$<$LF$>$\\}
\begin{itemize}
	\item ``SSS.S'' is the value of wind speed in $m/s$ with resolution of 1 decimal place.
	\item ``T'' used to describe true (aka absolute) readings.
	\item ``DDD.D'' is the angular position of the wind direction in degrees(\degree)
	\item ``M'' represent the base units $(m/s)$ for the wind speed.
	\item ``CC'' is the checksum for the NMEA telegram.
	\item The telegram terminating with a carriage return followed by a line feed.
\end{itemize}
The values of speed and direction is updated whenever the device acquire an new sample, and the sample is valid.
In the case of any invalid sample the last valid is used. \par
**The default values on boot is 0 $m/s$ and 0\degree.\par
**A new telegram transmitted every 1 sec via the RS-323 serial bus at Normal operation mode.
\newpage
\section{Configuration Interface}
The configuration interface of the device can be activated at the $\mu$C's boot procedure only.
The pin 4 of the ICSP header is checked and in case that is low the configuration interface function executed.
\begin{figure}[h!]
	\centering
	\includegraphics[width=2in]{../Artwork/pins.png}
	\caption{Jumper setup for Configuration Mode}
	\label{fig:pins}
\end{figure}
\par
A connection between pins 3 and 4 (like in figure \ref{fig:pins}) and a hard reboot can put the device in to configuration mode.
The configuration commands pass thru the device's RS-232 serial bus.
A crossover cable (null modem) is needed to connect any terminal equipment (DTE) to the device. \par
The baudrate of the RS-232 bus is 9600BPS with 1 start and stop bit and software handshaking (XON/XOFF).
\begin{table}[h!]
	\begin{center}
		\begin{tabular}{c|c}
			\textbf{Setup} & \textbf{Value} \\
			\hline
			Baudrate & 9600 BPS\\
			Handshake & XON/XOFF\\
			Start bit & 1 bit\\
			Stop bit & 1 bit\\
		\end{tabular}
		\caption{Device setup for Configuration Mode}
	\end{center}
\end{table}

In Configuration mode the firmware can accept only some specific ASCII characters. This includes:
\begin{enumerate}[]
	\item Any number N {\bf when} $0\leq N \leq9$ .
	\item Any lower and upper Latin letter.
	\item The equal (=) character.
	\item The minus (-) sign.
	\item The dollar (\$) character.
	\item The number (\#) character.
	\item The escape $<$ESC$>$ character.
	\item The carriage return $<$CR$>$ character.
	\item The line feed $<$LF$>$ character.
\end{enumerate}
**All the lower letters in converter to upper by the device's firmware. \par
In every successfully reception the device store the character in a buffer and echo it back, except in the cases of $<$ESC$>$, $<$CR$>$ and $<$LF$>$ characters.\\
The $<$CR$>$ and $<$LF$>$ characters used as command execution, and the $<$ESC$>$ character
as buffer reset.
A $<$CR$>$ followed from a $<$LF$>$ transmitted from the device in each excepted case.
\newpage
\subsection{Configuration parameters}
\label{subsec:param}
As it's have been mentioned in a previous sections the device have functionalities that required some configuration, thus some parameters need to be programmed.
The device provide access to this parameter by registers. The parameter's registers described in order below.
\begin{enumerate}[]
	\item Frequency to speed, numerator.
	\item Frequency to speed, denominator.
	\item Speed to mA low scale, numerator.
	\item Speed to mA low scale, denominator.
	\item Speed to mA high scale, numerator.
	\item Speed to mA high scale, denominator.
	\item Low to High threshold.
	\item High to Low threshold.
	\item Synchro output offset.
\end{enumerate}\par
Each parameter is stored in to the $\mu$C's internal EEPROM and loaded to RAM at boot.
In addition a Default parameters table is hardcoded in to program memory.
The purpose for this is to provide a recovery point for every unexpected situation. The values for the default parameters table derived below:
\begin{table}[h!]
  \begin{center}
    \begin{tabular}{c|r}
      \textbf{Default Parameter} & \textbf{Value} \\
      	\hline
	Frequency to speed, numerator & 25\\
	Frequency to speed, denominator & 521\\
	Speed to mA low scale, numerator & 35050\\
	Speed to mA low scale, denominator& 5607\\
	Speed to mA high scale, numerator& 35050\\
	Speed to mA high scale, denominator& 5277\\
	Low to High threshold& 115\\
	High to Low threshold& 100\\
	Synchro output offset&0\\
    \end{tabular}
  \end{center}
\end{table} \\
**The user is always good to keep a hard copy of any modified parameter.
\subsection{Configuration Commands}
The commands split in two groups. Start with {\bf``\$''} and start with {\bf``\#''}.
The {\bf``\#''} group is related to System and Calibration operations, and the {\bf``\$''} group to parameter configuration.\par
**Each command that user send to device must terminated with $<$CR$>$ or $<$LF$>$, otherwise will not execute. \par
**The Error led in to the configuration mode indicate the command execution, and in on while each command execution.\par
**A bunch of question marks transmitted from the device in case of a wrong command execution.
\pagebreak
\subsection{Command starting with ``\$''}
The commands then start ``\$'' is referred to parameters configuration. \\
A ``R'' or ``W'' followed to the ``\$'' specified if the operation is (R)ead or (W)rite.
\subsubsection{Write Parameters (``\$W'')}
\label{subsubpar:write_par}
The forms for this command is the follow:
{\center \$Wn=i$<$CR$>$ or $<$LF$>$ {\bf when} $ 0\leq n\leq 9$\\}
The n is the parameter's register number.
All the allowed values for n numbers described on the section \ref{subsec:param}.
The range for the number ``i'' in each case derived below.
\begin{table}[h!]
  \begin{center}
    \begin{tabular}{c||c|c|c}
      \textbf{n} & \textbf{Parameter} & \textbf{Range} &\textbf{Resolution}\\
      	\hline
	0 &(Relays output)& $\{0,1\}$ & Boolean\\
	1 &(Frequency to speed, numerator) & 0..65535 & unsigned 16bit\\
	2 &(Frequency to speed, denominator) & 0..65535 & unsigned 16bit\\
	3 &(Speed to mA low scale, numerator) & 0..65535 & unsigned 16bit\\
	4 &(Speed to mA low scale, denominator)& 0..65535 & unsigned 16bit\\
	5 &(Speed to mA high scale, numerator)& 0..65535 &unsigned 16bit\\
	6 &(Speed to mA high scale, denominator)& 0..65535 & unsigned 16bit\\
	7 &(Low to High threshold)& 0..255 & unsigned 8bit\\
	8 &(High to Low threshold)& 0..255 & unsigned 8bit\\
	9 &(Synchro output offset)& -72..0..72 & signed 8bit\\
    \end{tabular}
  \end{center}
\end{table}
\par
The parameter 0 is auxiliary and added to help on the calibration procedure by controlling the states of the relay outputs.
This parameter can be reads and written like the others parameters, but the value of it it can not be stored in to the EEPROM.
\par
The parameters 7 and 8 indicate the high and low threshold for hysteresis mechanism of the range change function,
the value of them always must have resolution of 1 decimal place and to be multiplied by 10.
eg. For 5.2m/s threshold the given parameter value is 52.
\par
The parameter 9 (Synchro output offset) have as argument a signed number. This value used as a compensation angle for the direction indicator.
The value of the parameter 9 can be a number between -72 to 72, each value correspond to a 2.5\degree offset. \\
eg. The value -36 correspond to a negative offset of  90\degree,
 $\displaystyle \left(\frac{ \text{-}90\degree}{2.5\degree}=\text{-}36\right)$.
 \par
** A EEPROM Write command (\#W) is required to store any change.
\subsubsection{Read Parameters (``\$R'')}
The forms for this command is the follow:
{\center \$Rn$<$CR$>$ or $<$LF$>$ {\bf when} $ 0\leq n\leq 9$\\}
The n is specifying which parameter will be Read.
The parameter 0 is referred to the state of the Relay outputs.
All the allowed n numbers following the order from section \ref{subsubpar:write_par}.
\par
**Is always good after the execution of a write to follow verification with a read of the written parameter's register.
\enlargethispage{\baselineskip}
\newpage
\subsection{Command starting with ``\#''}
The ``\#'' group of command refers to system and calibration operation. \\
The have a form as:
{\center \#$\ell$[=i]$<$CR$>$ or $<$LF$>$\\}
The ``$\ell$'' is an assistance letter that indicate the command's operation. Some command needs also a number i as argument.
The following subsections describe each entity of the ``\#'' group in details.
\\
** The string "UNKNOWN" transmitted in case of an unspecified $\ell$.

\subsubsection{The ``\#W''command}
Forming as:
{\center \#W$<$CR$>$ or $<$LF$>$\\}
This command (W)rite the current configuration parameters in to the $\mu$C's EEPROM.
\subsubsection{The ``\#L'' command}
Forming as:
{\center \#L$<$CR$>$ or $<$LF$>$\\}
This command (L)oad or restore the Default configuration parameters from the $\mu$C's EEPROM.
\subsubsection{The ``\#D'' command}
Forming as:
{\center \#D=$i<$CR$>$ or $<$LF$>$\\}
This command interact with the DAC's analog ports 1..3, which driving the (D)irection synchro outputs.
An $i$ number is required, with range of $0\leq i \leq 143$ for angle 0\degree to 357.5\degree.
\subsubsection{The ``\#C'' command}
Forming as:
{\center \#C=$i<$CR$>$ or $<$LF$>$\\}
This command interact with the DAC's analog ports 4, where is responsible for the (C)urrent output.
A number $i$ is required with range $0\leq i \leq (2^{12}-1)$ to control the output current.\\
The output current calculated as $\displaystyle I_{out}(mA)=\frac{i}{2^{12}}*5.845(mA)$
\subsubsection{The ``\#R'' command}
Forming as:
{\center \#R$<$CR$>$ or $<$LF$>$\\}
This command used to make a (R)eboot.
\subsubsection{The ``\#T'' command}
Forming as:
{\center \#T$<$CR$>$ or $<$LF$>$\\}
When this command received the device execute a Instrument (T)esting sequence. The test sequence described in the table follow. \\
\begin{table}[h!]
  \begin{center}
    \begin{tabular}{c|c|c}
      \textbf{Instrument under Test} & \textbf{Test condition} & \textbf{Run time (Est.)} \\
      	\hline
	Direction indicator (Desynn) & Set to 0\degree & 1 Sec\\
	Direction indicator (Desynn) &A Full CW revolution &10 Sec\\
	Wind speed indicator (Galvanometer) & Ramp up 0 to 12 m/s (Low scale) & 10 Sec\\
	Wind speed indicator (Galvanometer) & Zero mA output & 1 Sec\\
	Wind speed indicator (Galvanometer) & Ramp up to 12m/s (High scale) & 10 Sec\\
    \end{tabular}
  \end{center}
\end{table}\\
\enlargethispage{\baselineskip}
\newpage
\subsection{Configuration Example}
This subsection present a configuration example.
The wind speed tachometer sensor which will be used in this example produce a pulse output with frequency of $754Hz@70\bf{\sfrac{m}{s}}$.
\par
With the device out of power and the jumper setup like in figure \ref{fig:pins} the device is ready for configuration.
Then power applied to the device and a welcome message transmitted thru the RS-232 Port.
\begin{lstlisting}[frame=single,caption=The Welcome message]
Wind sensors Conditioner and NMEA0183 Transmitter
Made by Sam Harry Tzavaras
Source & Docs @ gitlab.com/fantomsam/wind_station
Copyright(C) 12019
---Config IF---
\end{lstlisting}
\subsubsection{Calibration}
The calibration procedure for the speed indicator can be done by sending ``$\#C$'' commands with an incrementing argument (to apply current to mA output),
and ``$\$W0$'' to change the Indicator's range (relay output).
This can help the operator to make two tables for each scale.
Each table compare the applied current code value with the reading of the indicator.
Using linearization to the table's data, two scalers can be extracted, one for each range.
This scalers have to be in a fraction form with units of current code for numerator and m/s for denominator.
A GCD \footnote{Grater Common Divider.} function is also good to applied on the fraction and then a factors reduction by divide each factors with the GCD.
\newpage
\subsubsection{Parameter loading}
The wind speed sensors parameters for the example is $\displaystyle \frac{35\sfrac{m}{s}}{377\text{ Hz}}$,
This derived from the defined sensor's scaler $\displaystyle{\left(\frac{70\sfrac{m}{s}}{754\text{ Hz}}\right)}$ with each factor divided by the $GCD(70,754)=2$. \par
To load the parameters the``\$W'' commands followed by ``\$R'' for verification. An example is derived bellow (Each sentence after \% is a comment).
\begin{lstlisting}[frame=single,caption=Loading parameters to the Device,numbers=left]
%load sensor's Scaler (Parameters 1,2)
$W1=35 %Device <- User
OK %Device -> User
$R1 %Device <- User
35 %Device -> User
$W2=377 %Device <- User
OK %Device -> User
$R1 %Device <- User
377 %Device -> User
%Load Indicator's Low range scaler (Parameters 3,4)
...
%Load Indicator's High range scaler  (Parameters 5,6)
...
%Instruments check
#T %Device <- User
OK %Device -> User
% if check is okay, Store To EEPROM, else repeat calibration
#W %Device <- User
OK %Device -> User
%Remove the jumber and reboot
#R
\end{lstlisting}

\newpage
\section{Appendixes}
\subsection{Wiring diagrams}
\label{subsec:wiring}
\begin{figure}[h!]
	\centering
	\includegraphics[width=\linewidth,angle=0]{../Artwork/wire_diag.png}
	\caption{Wind speed indicator with two ranges}
	\label{fig:wiring}
\end{figure}
The ``L1'' is a lamp (or LED) indicator that shows the current range, that lighting on the high range.
The components inside the doted box is the guts of a typical mechanical wind speed indicator with two ranges.
This include a $\mu$Ammeter and a shunting resistor network.
On low range the Contact COM-NC is closed and the series combination of R1 and R2 is applied parallel to the $\mu$Ammeter.
In this case the $\mu$Ammeter saturate in a low current due to the high shunting resistance.
On high range the Contact COM-NO is closed and force the R2 out of the shunting combination, thus the $\mu$Ammeter saturates in higher current.
\includepdf[pages=-,scale=.8,pagecommand={\subsection{Wind datalogger's Schematic diagram}},linktodoc=false,angle=90]
{../../Design_and_source/Hardware/datalogger/eagle_old/export/Blueprint_sch.pdf}
\includepdf[pages=-,scale=.9,pagecommand={\subsection{Dimensions}},linktodoc=false,angle=90]
{../../Design_and_source/Hardware/datalogger/eagle_old/export/Blueprint_PCB_dim.pdf}
\end{document}
