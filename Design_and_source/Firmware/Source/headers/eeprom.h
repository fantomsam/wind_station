/*
This file is part of The "Wind station Datalogger Firmware".

Wind station Datalogger Firmware is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This Firmware distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
in the repository of this project  If not, see <https://www.gnu.org/licenses/>.
*/

/*
 * File:   eeprom.h
 * Author: sam
 *
 * Created on April 28, 2019, 4:30 PM
 */

#ifndef EEPROM_H
#define	EEPROM_H
#define Read 255
#define Write 0
void delay_ms(unsigned long delay);
void RW_config(unsigned char *in_out,const unsigned char size,const unsigned char R_nW);

unsigned long readUSER_ID(void);//Function that read and return the USER_ID.

#endif	/* EEPROM_H */

