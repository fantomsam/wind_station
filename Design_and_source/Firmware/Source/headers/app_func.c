/*
This file is part of The "Wind station Datalogger Firmware".

Wind station Datalogger Firmware is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This Firmware distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
in the repository of this project  If not, see <https://www.gnu.org/licenses/>.
*/

#include <pic16regs.h>
#include <stdint.h>

#include "uC_def.h"
#include "AD5696R.h"
#include "app_usart.h"
#include "NMEA0183.h"
#include "eeprom.h"
#include "app_func.h"

/*
//calc the sin of the index
unsigned short LUT_calc(unsigned char index)
{
    //Sine quarter lookup table
    __code const unsigned short QSIN_LUT[]={0x0800,0x085A,0x08B4,0x090E,0x0966,0x09BF,0x0A16,
										    0x0A6C,0x0AC1,0x0B15,0x0B67,0x0BB8,0x0C06,0x0C53,
										  	0x0C9E,0x0CE6,0x0D2C,0x0D6F,0x0DB0,0x0DEE,0x0E29,
										  	0x0E60,0x0E95,0x0EC7,0x0EF5,0x0F1F,0x0F47,0x0F6A,
										  	0x0F8A,0x0FA6,0x0FBF,0x0FD3,0x0FE4,0x0FF1,0x0FF9,0x0FFE};
    if(index>144)
    	index%=144;
    switch(index)
    {
        case 36: return 0x0FFE; break;
        case 72: return 0x07FF; break;
        case 108: return 0; break;
        default:
            if(index<=35)
                return QSIN_LUT[index];
            else if(index>=37&&index<=71)
                return QSIN_LUT[35-(index-37)];
            else if(index>=73&&index<=107)
                return (0x0FFF-QSIN_LUT[index-72]);
            else if(index>=109&&index<=143)
                return (0x0FFF-QSIN_LUT[35-(index-109)]);
    }
    return -1;
}*/

//calc the cos of the index
unsigned short LUT_calc(unsigned char index)
{
    //Cosine quarter lookup table
    __code const unsigned short QCOS_LUT[]={0x0FFF,0x0FFE,0x0FF8,0x0FEE,0x0FE0,0x0FCF,0x0FBA,
                                            0x0FA1,0x0F84,0x0F64,0x0F40,0x0F18,0x0EED,0x0EBF,
                                            0x0E8D,0x0E58,0x0E20,0x0DE5,0x0DA8,0x0D67,0x0D24,
                                            0x0CDE,0x0C96,0x0C4C,0x0BFF,0x0BB1,0x0B61,0x0B0F,
                                            0x0ABC,0x0A67,0x0A12,0x09BB,0x0963,0x090B,0x08B2,0x0859};
    if(index>144)
    	index%=144;
    switch(index)
    {
		case 36: return 0x0800; break;
		case 72: return 0; break;
		case 108: return 0x0800; break;
		default:
			if(index<=35)
				return QCOS_LUT[index];
			else if(index>=37&&index<=71)
				return (0x0FFF-QCOS_LUT[35-(index-37)]);
			else if(index>=73&&index<=107)
				return (0x0FFF-QCOS_LUT[index-72]);
			else if(index>=109&&index<=143)
				return (QCOS_LUT[35-(index-109)]);
			break;
    }
    return 0x0FFF;
}


//Load the Synchro motor phase values to the DAC
void synchro_motor_out(unsigned char dir,signed char offset)
{
    dir+=offset;
    if(dir>143)
    {
		if(offset>=0)
			dir-=144;
		else
			dir= 144 -(255-dir);
    }
    AD5696_write(LUT_calc(dir),Out_PH1);
    AD5696_write(LUT_calc(dir-48),Out_PH2);//-48 -> 120° advance
    AD5696_write(LUT_calc(dir-96),Out_PH3);//-96 -> 240° advance
    DAC_L=0;//Load DAC values to output
    DAC_L=1;

}

//Load the Synchro motor phase values to the DAC
void mA_out(unsigned short val)
{
    AD5696_write(val,Out_uA);
    DAC_L=0;//Load DAC values to output
    DAC_L=1;

}
//Read the Direction value from the Shift register
unsigned char Dir_read(void)
{
    unsigned char ret=0,i;
    SCLK=0;
    //Load parallel data to shift register
    PLOAD=0;
    PLOAD=1;
    PLOAD=0;
    //read Shifted data
    for(i=0;i<8;i++)
    {
        if(DIN)
            ret|=0x80;
        //else
            //ret&=~0x80;
        if(i<7)
        {
            SCLK=1;
            ret>>=1;
            SCLK=0;
        }
    }
    return ret;
}

//convert a gray coded value to binary
unsigned char Gray_to_bin(unsigned char input)
{
    unsigned char i;
    for(i=4;i;i>>=1)
        input ^= (input >> i);
    if(input>71&&input<184)
        return 255;
    else if(input<=71)
        return input;
    else
        return (input-112);//modified for 144 sections with symmetry on 72th section
}
