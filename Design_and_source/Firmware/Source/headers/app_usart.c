/*
This file is part of The "Wind station Datalogger Firmware".

Wind station Datalogger Firmware is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This Firmware distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
in the repository of this project  If not, see <https://www.gnu.org/licenses/>.
*/

#include <pic16regs.h>

extern unsigned char UART_TX_buff[],UART_TX_buff_index;

void UART_Write(unsigned char data)
{
	while(!TRMT||TXIE);//wait in case of last unfinished transmission, and if buffered(by interrupt) TX in on going.
	TXREG = data;
}

void UART_Write_Text(unsigned char *text)
{
	while(*text!='\0')
	{
		UART_Write(*text);
		text++;
	}
}

void UART_Write_Text_buffered(void)
{
	UART_Write(UART_TX_buff[0]);
	UART_TX_buff_index=1;
	TXIE=1;//Enable UART interrupt on transmission.
}

