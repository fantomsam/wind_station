/*
This file is part of The "Wind station Datalogger Firmware".

Wind station Datalogger Firmware is free software: 
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This Firmware distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
in the repository of this project  If not, see <https://www.gnu.org/licenses/>.
*/
/* 
 * File:   app_func.h
 * Author: sam
 *
 * Created on May 8, 2019, 9:36 PM
 */

#ifndef APP_FUNC_H
#define	APP_FUNC_H

unsigned char Dir_read(void);
unsigned char Gray_to_bin(unsigned char input);
void synchro_motor_out(unsigned char dir,signed char offset);
void mA_out(unsigned short val);

#endif	/* APP_FUNC_H */

