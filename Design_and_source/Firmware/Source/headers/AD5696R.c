/*
This file is part of The "Wind station Datalogger Firmware".

Wind station Datalogger Firmware is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This Firmware distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
in the repository of this project  If not, see <https://www.gnu.org/licenses/>.
*/

#include "uC_def.h"
#include <pic16regs.h>
#include "AD5696R.h"
/*
Function: I2CInit
Return:
Arguments:
Description: Initialise I2C in master mode, Sets the required baudrate
*/
void I2C_Init(void)
{
    unsigned char i;
    //I2C bus recovery
    TRISB &= ~0x12;
    LATB3=0;
    if(!SDA)
    {
        for(i=0;i<9;i++)
        {
            SCL=0;
            __asm__ ("nop\nnop\nnop\nnop\n");
            SCL=1;
            __asm__ ("nop\nnop\nnop\nnop\n");
        }
    }
    TRISB |= 0x12;
    SSP1STAT &= ~0x80;  /* Slew rate enable */
	SSP1CON1 = 0x28;    /* SSPEN = 1, I2C Master mode, clock = FOSC/(4 * (SSPADD + 1)) */
	SSP1ADD = 6;    	/* 396.26kHz @ 11.0592Mhz Fosc */
}

/*
Function: I2CStart
Return:
Arguments:
Description: Send a start condition on I2C Bus
*/
void I2CStart(void)
{
	SEN = 1;         /* Start condition enabled */
	while(SEN);      /* automatically cleared by hardware */
    return;          /* wait for start condition to finish */
}

/*
Function: I2CReStart
Return:
Arguments:
Description: Send a restart condition on I2C Bus
*/
/*
void I2CRestart(void)
{
	RSEN = 1;        // Restart condition enabled
	while(RSEN);     // automatically cleared by hardware
    return;          // wait for start condition to finish
}
*/

/*
Function: I2CStop
Return:
Arguments:
Description: Send a stop condition on I2C Bus
*/
void I2CStop(void)
{
	PEN = 1;         /* Stop condition enabled */
	while(PEN);      /* PEN automatically cleared by hardware */
    return;          /* Wait for stop condition to finish */
}

/*
Function: I2CWait
Return:
Arguments:
Description: wait for transfer to finish
*/
void I2CWait(void)
{
	while((SSP1CON2 & 0x1F)||(SSP1STAT & 0x04));
	return; /* wait for any pending transfer */
}

/*
Function: I2CSend
Return: ACK bit
Arguments: dat - 8-bit data to be sent on bus
           data can be either address/data byte
Description: Send 8-bit data on I2C bus
*/
unsigned char I2CSend(unsigned char dat)
{
	unsigned char ACK_ret;
    SSP1BUF = dat;    		/* Move data to SSPBUF */
	while(SSP1STAT&0x01);   /* wait till complete data is sent from buffer */
    ACK_ret=ACKSTAT;
	I2CWait();       		/* wait for any pending transfer */
    return ACK_ret;
}

/*
Function: I2CReceive
Return: Received Data
Arguments:
Description: Send 8-bit data on I2C bus
*/
/*
unsigned char I2CReceive(void)
{
  RCEN = 1; 			// Enable & Start Reception
  while(SSP1STAT&0x01); // Wait Until Completion
  I2CWait();
  return SSPBUF; 		// Return The Received Byte
}
*/

/*
Function: AD5696_write
Return: ACK bit
Arguments:
	Value: 16-bit data to be sent on AD5696R
	Channel: The analog channel that the value will be sent
Description: Function to send data to specified channel of the AD5696, The channel must have value A..D
*/
unsigned char AD5696_write(unsigned short Value,unsigned char Channel)
{
    unsigned char error;
    I2CStart();
    if(!(error=I2CSend(0x18)))
    {
		I2CSend(0x10|(Channel&0x0F));
		I2CSend(Value>>4);
		I2CSend(Value<<4);
	}
	I2CStop();
    return error;
}
