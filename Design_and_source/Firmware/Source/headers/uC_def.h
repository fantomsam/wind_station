/*
This file is part of The "Wind station Datalogger Firmware".

Wind station Datalogger Firmware is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This Firmware distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
in the repository of this project  If not, see <https://www.gnu.org/licenses/>.
*/

#define _XTAL_FREQ 11059200

//Pins that manipulated from software
#define SCL RB4
#define DAC_L LATB3
#define SDA RB1
#define PLOAD LATA4
#define DIN RA3
#define SCLK LATA2
#define ERROR_LED LATA1
#define RELAY LATA0
#define CONFIG RB7

//structs datatypes
typedef union{
    struct tag_flags_bits{
		unsigned update_Freq:1;
		unsigned no_signal:1;
		unsigned UART_new_RX_message:1;
    }flags_bits;
    unsigned char flags_reg;
}app_flags;

typedef struct configuration_words{
    unsigned short freq_to_speed_num;
    unsigned short freq_to_speed_den;
    unsigned short speed_to_mA_low_num;
    unsigned short speed_to_mA_low_den;
    unsigned short speed_to_mA_high_num;
    unsigned short speed_to_mA_high_den;
    unsigned char Low_to_High;
    unsigned char High_to_Low;
    signed char synchro_motor_offset;
    unsigned char First_config;
}config;

