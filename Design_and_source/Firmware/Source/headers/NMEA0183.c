/*
This file is part of The "Wind station Datalogger Firmware".

Wind station Datalogger Firmware is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This Firmware distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
in the repository of this project  If not, see <https://www.gnu.org/licenses/>.
*/

#include <pic16regs.h>

#include "NMEA0183.h"

unsigned char app_strcpy(unsigned char *out,const unsigned char *in)
{
    unsigned char ret=0;
    while(in[ret]!='\0')
    {
	    out[ret]=in[ret];
	    ret++;
    }
    return ret;
}

void checksumtostr(unsigned char *out,const unsigned char input)
{
    unsigned char i;
    out[0]=(input>>4)&0x0F;
    out[1]=input&0x0F;
    for(i=0;i<2;i++)
    {
	    if(out[i]<10)
	        out[i]+='0';
	    else
	        out[i]+='A'-10;
    }
    out[i]='\0';
}

void bin_to_NMEA_val(unsigned char *out,unsigned short val)
{
    unsigned char c=0;
    out[5]='\0';
    for(c=4;c!=0xff;c--)
    {
	    if(c==3)
	        out[c]='.';
	    else
	    {
	        out[c]=val%10+'0';
	        val/=10;
	    }
    }
}

unsigned char NMEA_checksum_add_vars(const unsigned char *val)
{
    unsigned char c=0,i=0;
    while(val[i])
    {
        c ^= val[i];
        i++;
    }
    return c;
}

void NMEA_build(unsigned char *out, unsigned short speed, unsigned short direction)
{
    unsigned char checksum = 0x4B,i;
    unsigned char str_speed[6],str_direction[6],str_checksum[3];
    bin_to_NMEA_val(str_speed,speed);
    bin_to_NMEA_val(str_direction,direction);
    checksum^=NMEA_checksum_add_vars(str_speed)^NMEA_checksum_add_vars(str_direction);
    checksumtostr(str_checksum,checksum);
    //copy the first 6bytes of the NMEA message "$WIMWV,"
    i=app_strcpy(out,"$WIMWV,");
    //copy speed value string
    i+=app_strcpy((out+i),str_speed);
    //copy next 3 bytes of the NMEA message ",T,"
    i+=app_strcpy((out+i),",T,");
    //copy direction value string
    i+=app_strcpy((out+i),str_direction);
    //copy next 3 bytes of the NMEA message ",M*"
    i+=app_strcpy((out+i),",M*");
    //copy direction value string
    i+=app_strcpy((out+i),str_checksum);
    //add "\r\n"
    app_strcpy((out+i),"\r\n");
    out[27]='\0';
    return;
}
