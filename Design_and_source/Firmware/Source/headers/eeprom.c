/*
This file is part of The "Wind station Datalogger Firmware".

Wind station Datalogger Firmware is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This Firmware distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
in the repository of this project  If not, see <https://www.gnu.org/licenses/>.
*/
#include <pic16regs.h>

#include "eeprom.h"

//software delay
void delay_ms(unsigned long delay)
{
    unsigned long i;
    while(delay)
    {
        for(i=0;i<74;i++);
        delay--;
    }
}

unsigned char readEEPROM(unsigned short address)
{
  EEADR = address; //Address to be read
  EEPGD = 0;//Selecting EEPROM Data Memory
  CFGS = 0;//Access EEPROM Data Memory
  RD = 1; //Initialise read cycle
  return EEDAT; //Returning data
}

unsigned long readUSER_ID(void)
{
	unsigned char i=0;
	unsigned long ret = 0;

	EECON1 = 0;
	EEPGD = 1;//Selecting Flash memory
  	CFGS = 1;//Access Configuration, User ID and Device ID Registers

	for(i=0; i<4; i++)
	{
		EEADR = _IDLOC0 + i; //Address of User ID byte to read
		RD = 1; //Initialise read cycle
		ret = EEDAT<<8*i;
	}
	return ret;
}

void writeEEPROM(unsigned char address, unsigned char data)
{
  unsigned char INTCON_SAVE;//To save INTCON register value
  EEADR = address; //Address to write
  EEDAT = data; //Data to write
  EEPGD = 0; //Selecting EEPROM Data Memory
  CFGS = 0;//Access EEPROM Data Memory
  WREN = 1; //Enable writing of EEPROM
  INTCON_SAVE=INTCON;//Backup INCON interrupt register
  INTCON=0; //Disables the interrupt
  EECON2=0x55; //Required sequence for write to internal EEPROM
  EECON2=0xAA; //Required sequence for write to internal EEPROM
  WR = 1; //Initialize write cycle
  INTCON = INTCON_SAVE;//Enables Interrupt
  WREN = 0; //To disable write
  while(!EEIF);//wait until writing operation complete
  EEIF = 0; //Clearing EEIF bit
}


void RW_config(unsigned char *in_out,const unsigned char size,const unsigned char R_nW)
{
    unsigned char addr;
    for(addr=0; addr<size; addr++)
    {
	    if(R_nW)
	        in_out[addr] = readEEPROM(addr);
	    else
	        writeEEPROM(addr, in_out[addr]);
    }
    return;
}
