/*
This file is part of The "Wind station Datalogger Firmware".

Wind station Datalogger Firmware is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This Firmware distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
in the repository of this project  If not, see <https://www.gnu.org/licenses/>.
*/
/*
 * File:   AD5696R.h
 * Author: Sam
 *
 * Created on April 27, 2019, 1:21 PM
 */

#ifndef AD5696R_H
#define	AD5696R_H

#define Out_PH3 0x01
#define Out_PH2 0x02
#define Out_PH1 0x04
#define Out_uA  0x08
#define ALL_CH  0x0F

void I2C_Init(void);
unsigned char AD5696_write(unsigned short value,unsigned char Channel);
#endif	/* AD5696R_H */

