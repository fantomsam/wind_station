/*
This file is part of The "Wind station Datalogger Firmware".

Wind station Datalogger Firmware is free software: 
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This Firmware distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
in the repository of this project  If not, see <https://www.gnu.org/licenses/>.
*/
/* 
 * File:   NMEA0183.h
 * Author: sam
 *
 * Created on April 27, 2019, 6:39 PM
 */

#ifndef NMEA0183_H
#define	NMEA0183_H

void NMEA_build(char *out,unsigned short speed, unsigned short direction);

#endif	/* NMEA0183_H */

