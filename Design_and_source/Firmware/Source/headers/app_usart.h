/*
This file is part of The "Wind station Datalogger Firmware".

Wind station Datalogger Firmware is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This Firmware distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
in the repository of this project  If not, see <https://www.gnu.org/licenses/>.
*/

/*
 * File:   app_usart.h
 * Author: sam
 *
 * Created on April 27, 2019, 5:27 PM
 */

#ifndef APP_USART_H
#define	APP_USART_H

//ASCII control characters
#define XON 0x11
#define XOFF 0x13
#define ACK 0x06
#define NACK 0x15

void UART_Write(char data);
void UART_Write_Text(char *text);
void UART_Write_Text_buffered(void);

#endif	/* APP_USART_H */

