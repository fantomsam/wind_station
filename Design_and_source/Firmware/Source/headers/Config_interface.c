/*
This file is part of The "Wind station Datalogger Firmware".

Wind station Datalogger Firmware is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This Firmware distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
in the repository of this project  If not, see <https://www.gnu.org/licenses/>.
*/
#define bin_to_str_size 10

#include <pic16regs.h>

#include "uC_def.h"
#include "AD5696R.h"
#include "app_usart.h"
#include "eeprom.h"
#include "app_func.h"

//External variables
extern app_flags flags;
extern config eeprom_config_words;
extern config default_eeprom_config_words;
extern unsigned char UART_RX_buff[];

//configuration functions
void instrument_test(void);
void write_decoder(const unsigned char in_str[]);
void read_decoder(const unsigned char in_str[]);
unsigned short str_to_bin(const unsigned char in_str[]);

void config_interface(void)
{
    unsigned char i;
    //Send Welcome message
    UART_Write_Text("\r\n"
    				"Wind sensors Conditioner and NMEA0183 Transmitter\r\n"
    				"Made by Sam Harry Tzavaras\r\n"
    				"Src&Docs@gitlab.com/fantomsam/wind_station\r\n"
    				"🄯12019-24\r\n"
    				"\t---Config IF---\r\n");
    //Interrupt init
    PEIE=1;//Peripherals Interrupts Enable
    RCIE=1;//USART receive interrupt enable

    //USART Init for configuration
    CREN = 1;//Enables USART Reception

    //Start uC peripherals
    GIE=1;//General interrupt enable

    //Set instruments
    synchro_motor_out(0,eeprom_config_words.synchro_motor_offset);
    mA_out(0);
    while(1)
    {
        if(flags.flags_bits.UART_new_RX_message)
		{
			GIE=0;
	        UART_Write(XOFF);
	        ERROR_LED=1;
			for(i=0; !(UART_RX_buff[i]=='\0'||UART_RX_buff[i]=='#'||UART_RX_buff[i]=='$'); i++)
			;
			if(UART_RX_buff[i]=='#')
			{
				switch(UART_RX_buff[i+1])
				{
					case'W': UART_Write_Text("\r\nWrite Configuration to EEPROM\r\n");
				             RW_config((unsigned char*)eeprom_config_words, sizeof(eeprom_config_words), Write);
							 break;
					case'L': UART_Write_Text("\r\nLoad Default to EEPROM\r\n");
				             RW_config((unsigned char*)default_eeprom_config_words, sizeof(default_eeprom_config_words), Write);
							 RW_config((unsigned char*)eeprom_config_words, sizeof(eeprom_config_words), Read);
						     break;
					case'D': synchro_motor_out(str_to_bin(UART_RX_buff+i+3), eeprom_config_words.synchro_motor_offset);
							 break;
					case'C': mA_out(str_to_bin(UART_RX_buff+i+3));
							 break;
				    case'R': UART_Write(XON);
							 while(!TRMT);
				    		 Reset();
				    		 break;
					case'T': UART_Write_Text("\r\nTesting...\r\n");
							 instrument_test();
							 break;
					default: UART_Write_Text("\r\nUNKNOWN\r\n");
				}
				UART_Write_Text("\r\nOK\r\n");
			}
			else if(UART_RX_buff[i]=='$')
			{
				switch(UART_RX_buff[i+1])
				{
					case'W': write_decoder(UART_RX_buff+(i+2)); break;
					case'R': read_decoder(UART_RX_buff+(i+2)); break;
					default :UART_Write_Text("\r\nUNKNOWN\r\n");
				}
			}
			else
				UART_Write_Text("\r\n????\r\n");
			flags.flags_bits.UART_new_RX_message=0;
			//Reset USART unit
			SPEN=0;
			SPEN=1;
			//ReEnable interrupt
			GIE=1;
			UART_Write(XON); //Handshake with PC
		    ERROR_LED=0;
		}
    }
}


//Test function for unit's instruments
void instrument_test(void)
{
	unsigned short i, Calc_reg;

	//Check synchro motor
	synchro_motor_out(0,eeprom_config_words.synchro_motor_offset);
	delay_ms(1000);
	for(i=0;i<144;i++)
	{
		synchro_motor_out(i,eeprom_config_words.synchro_motor_offset);
		delay_ms(70);
	}
	delay_ms(1000);
	//Check Wind speed instrument (galvanometer) low scale
	RELAY=0;
	for(i=0;i<=120;i++)
	{
		Calc_reg=((unsigned long)i*eeprom_config_words.speed_to_mA_low_num)/eeprom_config_words.speed_to_mA_low_den;
		mA_out(Calc_reg);
		delay_ms(82);
	}
	delay_ms(1000);
	mA_out(0);
	delay_ms(1000);
	//Check Wind speed instrument (galvanometer) High scale
	RELAY=1;
	for(i=0;i<=600;i++)
	{
		Calc_reg=((unsigned long)i*eeprom_config_words.speed_to_mA_high_num)/eeprom_config_words.speed_to_mA_high_den;
		mA_out(Calc_reg);
		delay_ms(16);
	}
	delay_ms(1000);
	mA_out(0);
	RELAY=0;
}

// string to signed binary number conversion
unsigned short str_to_bin(const unsigned char in_str[])
{
    unsigned short ret=0;
    unsigned char i;

	for(i=(in_str[0]=='-')?1:0; in_str[i]!='\0'; i++)
	{
		if((in_str[i]>='0' && in_str[i]<='9'))
			ret=ret*10+(in_str[i]-'0');
		else
			break;
	}
	if(in_str[0]=='-')
		return -ret;
	else
		return ret;
}

// signed binary to string conversion
void bin_to_str(unsigned char out_str[], signed long in)
{
	unsigned char i=0, j=0, t;
	unsigned short in_data;

	if(in<0)
	{
		out_str[0]='-';
		in=-in;
		i++;
		j++;
	}
	in_data=in;
	do{
		out_str[i]=in_data%10+'0';
		in_data = in_data/10;
		i++;
	}while(in_data && i<bin_to_str_size-1);
	out_str[i]='\0';
	for(i--;i>j;j++,i--)
	{
		t=out_str[i];
		out_str[i]=out_str[j];
		out_str[j]=t;
	}
}

void write_decoder(const unsigned char in_str[])
{
    unsigned short num;
    if(in_str[0]<'0'||in_str[0]>'9'||in_str[1]!='=')
    {
		UART_Write_Text("\r\nREG_error\r\n");
		return;
    }
    num = str_to_bin(in_str+2);
    switch(in_str[0])
    {
		case'0': RELAY = num; break;
		case'1': eeprom_config_words.freq_to_speed_num = num; break;
		case'2': eeprom_config_words.freq_to_speed_den = num; break;
		case'3': eeprom_config_words.speed_to_mA_low_num = num; break;
		case'4': eeprom_config_words.speed_to_mA_low_den = num; break;
		case'5': eeprom_config_words.speed_to_mA_high_num = num; break;
		case'6': eeprom_config_words.speed_to_mA_high_den = num; break;
		case'7': eeprom_config_words.Low_to_High = num; break;
		case'8': eeprom_config_words.High_to_Low = num; break;
		case'9': eeprom_config_words.synchro_motor_offset = num;
		         synchro_motor_out(0, eeprom_config_words.synchro_motor_offset);
		         break;
    }
    UART_Write_Text("\r\nOK\r\n");
}
void read_decoder(const unsigned char in_str[])
{
	unsigned char temp_tx_buff[bin_to_str_size];

	UART_Write_Text("\r\n");
	switch(in_str[0])
	{
		case'0': bin_to_str(temp_tx_buff, RELAY); break;
		case'1': bin_to_str(temp_tx_buff, eeprom_config_words.freq_to_speed_num); break;
		case'2': bin_to_str(temp_tx_buff, eeprom_config_words.freq_to_speed_den); break;
		case'3': bin_to_str(temp_tx_buff, eeprom_config_words.speed_to_mA_low_num); break;
		case'4': bin_to_str(temp_tx_buff, eeprom_config_words.speed_to_mA_low_den); break;
		case'5': bin_to_str(temp_tx_buff, eeprom_config_words.speed_to_mA_high_num); break;
		case'6': bin_to_str(temp_tx_buff, eeprom_config_words.speed_to_mA_high_den); break;
		case'7': bin_to_str(temp_tx_buff, eeprom_config_words.Low_to_High); break;
		case'8': bin_to_str(temp_tx_buff, eeprom_config_words.High_to_Low); break;
		case'9': bin_to_str(temp_tx_buff, eeprom_config_words.synchro_motor_offset); break;
		default: UART_Write_Text("\r\n????\r\n"); return;
	}
	UART_Write_Text(temp_tx_buff);
	delay_ms(10);
	UART_Write_Text("\r\n");
}
