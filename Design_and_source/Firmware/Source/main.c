/*
This file is part of The "Wind station Datalogger Firmware".

Wind station Datalogger Firmware is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This Firmware distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
in the repository of this project  If not, see <https://www.gnu.org/licenses/>.
*/

/*
 * File:   main.c
 * Author: Sam Harry Tzavaras
 *
 * Created on April 27, 2019, 5:27 PM
 */

/* Pin's Designator | Name | Direction
 * RA0 Relay_Driver Out
 * RA1 Error_LED Out
 * RA2 Par2Ser_Clock Out
 * RA3 Par2Ser_Data_Out In
 * RA4 Par2Ser_Load Out
 * RB0 Wind_Speed_sensor In
 * RB1 SDA In/Out
 * RB2 USART_RX In
 * RB3 DAC_Sync Load Out
 * RB4 SCL Out
 * RB5 USART TX Out
 * RB6 PGC In
 * RB7 PGD/CONFIG In/Out - IN
 */

#define EEPROM_ADDR 0xF000

#define EEPROM_CONFIG_FIRST_CONFIG 0x55

#define TMR1_overflow_limit 2
#define freq_no_signal_cnt_limit 2 //for approx 2 sec
#define MAX_dir_seg_value 144 //144 is the maximum value for the segments of the direction sensor.
#define NMEA_cnt_to_send_init 43 //for approx 1 sec

#include <pic16regs.h>
#include <stdint.h>

#include "headers/uC_def.h"
#include "headers/AD5696R.h"
#include "headers/app_usart.h"
#include "headers/NMEA0183.h"
#include "headers/eeprom.h"
#include "headers/app_func.h"

//Fuses Init
/*
	CONFIG1
	  FOSC = XT        // Oscillator Selection (XT Oscillator, Crystal/resonator connected between OSC1 and OSC2 pins)
	  WDTE = OFF       // Watchdog Timer Enable (WDT enabled)
	  PWRTE = OFF      // Power-up Timer Enable (PWRT disabled)
	  MCLRE = ON       // MCLR Pin Function Select (MCLR/VPP pin function is MCLR)
	  CP = OFF         // Flash Program Memory Code Protection (Program memory code protection is disabled)
	  CPD = OFF        // Data Memory Code Protection (Data memory code protection is disabled)
	  BOREN = ON       // Brown-out Reset Enable (Brown-out Reset enabled)
	  CLKOUTEN = OFF   // Clock Out Enable (CLKOUT function is disabled. I/O or oscillator function on the CLKOUT pin)
	  IESO = ON        // Internal/External Switchover (Internal/External Switchover mode is enabled)
	  FCMEN = ON       // Fail-Safe Clock Monitor Enable (Fail-Safe Clock Monitor is enabled)

	CONFIG2
	  WRT = OFF        // Flash Memory Self-Write Protection (Write protection off)
	  PLLEN = OFF      // PLL Enable (4x PLL disabled)
	  STVREN = ON      // Stack Overflow/Underflow Reset Enable (Stack Overflow or Underflow will cause a Reset)
	  BORV = LO        // Brown-out Reset Voltage Selection (Brown-out Reset Voltage (Vbor), low trip point selected.)
	  LVP = ON         // Low-Voltage Programming Enable (Low-voltage programming enabled)
*/
__code uint16_t __at (_CONFIG1) configword1 = _FOSC_HS & _WDTE_OFF & _PWRTE_ON & _MCLRE_ON & _CP_OFF & _CPD_OFF & _BOREN_ON & _CLKOUTEN_OFF & _IESO_OFF & _FCMEN_ON;
__code uint16_t __at (_CONFIG2) configword2 = _WRT_OFF & _PLLEN_OFF & _STVREN_ON & _BORV_LO & _LVP_OFF;

//Firmware revision.
//__code const char __at (_IDLOC0) Firm_Ver = 1;

//Global Variables
//Default configuration, for factory reset
__code config default_eeprom_config_words = {
	.First_config=EEPROM_CONFIG_FIRST_CONFIG,
	.freq_to_speed_num=25,
	.freq_to_speed_den=521,
	.speed_to_mA_low_num=35050,
	.speed_to_mA_low_den=5607,
	.speed_to_mA_high_num=35050,
	.speed_to_mA_high_den=5277,
	.synchro_motor_offset=0,
	.Low_to_High=115,
	.High_to_Low=100
};
/*
__code config __at (EEPROM_ADDR) EEPROM_config = {
	.First_config=0,
	.freq_to_speed_num=25,
	.freq_to_speed_den=521,
	.speed_to_mA_low_num=35050,
	.speed_to_mA_low_den=5607,
	.speed_to_mA_high_num=35050,
	.speed_to_mA_high_den=5277,
	.synchro_motor_offset=0,
	.Low_to_High=115,
	.High_to_Low=100
};
*/
app_flags flags;//System global flags
//For USART
unsigned char UART_RX_buff[20],UART_RX_buff_index=0,
			  UART_TX_buff[28],UART_TX_buff_index=0,
			  NMEA_cnt_to_send=NMEA_cnt_to_send_init;
//For Frequency measure
unsigned char TMR1_overflow_cnt=0;
unsigned char freq_no_signal_cnt=0;
//Instruments Scalers and offsets
config eeprom_config_words;


//ISR function
void ISR_function(void) __interrupt(0)
{
    unsigned char i;
    GIE=0;
    if(TMR1IF)
    {
		if(TMR1_overflow_cnt<TMR1_overflow_limit)
			TMR1_overflow_cnt++;
		else
		{
			flags.flags_bits.update_Freq = 1;//set frequency update flag
			TMR1ON=0;//Stop timer
			TMR1GIF=0;
		}
		freq_no_signal_cnt=0;//zero no signal counter
		TMR1IF=0;
    }
    if(TMR1GIF)
    {
		freq_no_signal_cnt=0;//zero no signal counter
		flags.flags_bits.update_Freq=1;//set frequency update flag
		TMR1ON=0;//Stop timer
		TMR1GIF=0;
		TMR1IF=0;
    }
    if(TMR0IF)
    {
		if(NMEA_cnt_to_send)
		{
			NMEA_cnt_to_send--;
			if(!NMEA_cnt_to_send)
			{	//Mechanism that indicate no signal on TMR1
				if(freq_no_signal_cnt>=freq_no_signal_cnt_limit)
				{
					flags.flags_reg=0x03;//set update_Freq and no_signal flags
					freq_no_signal_cnt=0;//zero no signal counter
				}
				else
					freq_no_signal_cnt++;
			}
		}
		TMR0IF=0;
    }
    if(RCIF)
    {
        UART_RX_buff[UART_RX_buff_index]=RCREG;//Read received character
        //Check for newline or carriers return characters
        if(UART_RX_buff[UART_RX_buff_index]=='\n'||UART_RX_buff[UART_RX_buff_index]=='\r')
        {
            UART_RX_buff[UART_RX_buff_index]='\0';
            UART_RX_buff_index=0;
            flags.flags_bits.UART_new_RX_message=1;
            return;
        }
        else if(UART_RX_buff[UART_RX_buff_index]=='\e')//Check for ESC.
        {
            UART_RX_buff_index=0;
            UART_Write_Text("\r\n");
        }
        else
        {
            //Convert lower to upper
            if(UART_RX_buff[UART_RX_buff_index]>='a'&&UART_RX_buff[UART_RX_buff_index]<='z')
                UART_RX_buff[UART_RX_buff_index]+='A'-'a';
            //Check if received character is in range
            if((UART_RX_buff[UART_RX_buff_index]>='A'&&UART_RX_buff[UART_RX_buff_index]<='Z')
              ||UART_RX_buff[UART_RX_buff_index]>='0'&&UART_RX_buff[UART_RX_buff_index]<='9'
              ||UART_RX_buff[UART_RX_buff_index]=='-'||UART_RX_buff[UART_RX_buff_index]=='='
              ||UART_RX_buff[UART_RX_buff_index]=='#'||UART_RX_buff[UART_RX_buff_index]=='$')
            {
                UART_Write(UART_RX_buff[UART_RX_buff_index]);//Echo received character back
                if(UART_RX_buff_index>=(sizeof(UART_RX_buff)-2))//buffer overflow check
                {
                    for(i=0;i<(sizeof(UART_RX_buff));i++)
                        UART_RX_buff[i] = UART_RX_buff[i+1];
                }
                else
                    UART_RX_buff_index++;
            }
        }
    }
	if(TXIE && TXIF)// && TRMT)
	{
		if(UART_TX_buff[UART_TX_buff_index]=='\0'||UART_TX_buff_index>=sizeof(UART_TX_buff))
		{
			UART_TX_buff_index=0;
			TXIE=0;//Disable UART interrupt on transmission.
		}
		else
		{
			TXREG = UART_TX_buff[UART_TX_buff_index];
			UART_TX_buff_index++;
		}
	}
    GIE=1;
    return;
}

//Configuration interface Function Definition
void config_interface(void);

void main(void)
{
	unsigned short *speed_to_mA_num,*speed_to_mA_den,Wind_speed=0;
	unsigned char t_dir_val,dir_val=0;
	unsigned long Calc_reg=0, Firm_ver;

    //Start_up init
    OSCCON = 0;
    TRISA = 0b11101000;//Set Direction of I/O pins on PORTA
    TRISB = 0b11000111;//Set Direction of I/O pins on PORTB
    APFCON0 = 0b10000000;//Set USART module RX on pin RB2
    APFCON1 = 0b00000001;//Set USART module TX on pin RB5
    OPTION_REG&=0x7F;//weak pull up enable
    WPUB=0xFF;//enable weak pull up on port B
    //INIT output pins
    ERROR_LED=0;
    RELAY=0;
    DAC_L=1;
    ANSELA = 0;//Disable Analog pins PORTA
    ANSELB = 0;//Disable Analog pins PORTB
    flags.flags_reg=0;
    delay_ms(2000);
    //USART Init
    SPBRG = 71;//11 For 57.6 kbps, 71 for 9.6 kbps, 142 for 4.8 kbps.
    SYNC = 0;//Setting Asynchronous Mode
    BRGH = 1;//High speed Selected
    SPEN = 1;//Enables Serial Port
    CREN = 0;//Disable Reception
    TXEN = 1;//Enables USART Transmission

    Firm_ver = readUSER_ID();
    RW_config((unsigned char *)eeprom_config_words, sizeof(eeprom_config_words), Read);
    if(eeprom_config_words.First_config != EEPROM_CONFIG_FIRST_CONFIG)
    {
        RW_config((unsigned char *)default_eeprom_config_words, sizeof(default_eeprom_config_words), Write);
        delay_ms(100);
	    RW_config((unsigned char *)eeprom_config_words, sizeof(eeprom_config_words), Read);
    }

    //Init I2C module
    I2C_Init();

    if(!CONFIG) //Check Configuration pins
        config_interface();

    //TMR0_init
    TMR0CS=0; //Set Timer clock source to instructions clock
    PSA=0; //Enable prescaler
    OPTION_REG |= 0x07;//TMR0 prescaler to 1:256

    //TMR1 Init
    T1GSS0=0;//Set Timer Gate from external pin T1G
    T1GSS1=0;
    T1GPOL=0;//Gate negative polarity
    T1GTM=1;//Gate Period measure mode
    T1GSPM=1;//Enable single pulse acquisition
    T1GGO=1;//Set single pulse acquisition GO function
    TMR1GE=1;//Set Gate module ON
    T1OSCEN=0;//disable Timer external oscillator
    //Set Timer's clock to system Clock(Fosc/4)
    TMR1CS0=0;
    TMR1CS1=0;
    //Set Timer Prescaler to 1/8
    T1CKPS0=1;
    T1CKPS1=1;
    NOT_T1SYNC=0;//timer sync with system.

    //Interrupt init
    PEIE=1;//Peripherals Interrupts Enable
    TMR1GIE=1;//Timer 1 Gate interrupt enable
    TMR1GIF=0;//Clear Timer 1 Gate interrupt flag
    TMR1IE=1;//Timer 1 overflow interrupt enable
    TMR1IF=0;//Clear Timer 1 overflow interrupt flag
    TMR0IE=1;//Timer 0 overflow interrupt enable

    //Init Instruments
    synchro_motor_out(0,eeprom_config_words.synchro_motor_offset);
    mA_out(0);
    //Start mA_out in low scale
    speed_to_mA_num = &(eeprom_config_words.speed_to_mA_low_num);
    speed_to_mA_den = &(eeprom_config_words.speed_to_mA_low_den);
    //Start uC peripherals
    GIE=1;//General interrupt enable
    //Reset Timer count register
    TMR1H=0;
    TMR1L=0;
    TMR1ON=1;//Start timer

    //Main application loop
    while(1)
    {
        //Read Direction form parallel bus and convert gray to bin
        if((t_dir_val=Gray_to_bin(Dir_read()))<MAX_dir_seg_value)
        {
            dir_val=t_dir_val;
	    	//Send Direction_to_synchro values to DAC
            synchro_motor_out(dir_val,eeprom_config_words.synchro_motor_offset);
            ERROR_LED=0;
        }
        else
            ERROR_LED=1;
        //Read Frequency, new sample
        if(flags.flags_bits.update_Freq)
        {
        	//Very low frequency or no signal check
			if(TMR1_overflow_cnt>=TMR1_overflow_limit||flags.flags_bits.no_signal)
			{
				flags.flags_bits.no_signal=0;
				Calc_reg=0;//Zero Frequency value
			}
			else
			{   //Calculate and scale Measured Frequency
		        Calc_reg = (((unsigned short)TMR1H)<<8)|TMR1L;//Calculate total counts
		        if(TMR1_overflow_cnt)
		        	Calc_reg |= ((unsigned long)TMR1_overflow_cnt<<16);//Add overflow value to counts
				Calc_reg = 3456000/Calc_reg; //Scale to Fosc/(8*4), x10 for dHz resolution
		        //Wind speed scaling calculation
		        Calc_reg=(Calc_reg*eeprom_config_words.freq_to_speed_num)/eeprom_config_words.freq_to_speed_den;
			}
			Wind_speed=Calc_reg;
			//Wind_speed to mA_out calculation, and (High,low) scale selection
			if(Wind_speed>eeprom_config_words.Low_to_High)
			{
				RELAY=1;
				speed_to_mA_num = &(eeprom_config_words.speed_to_mA_high_num);
				speed_to_mA_den = &(eeprom_config_words.speed_to_mA_high_den);
			}
			else if(Wind_speed<eeprom_config_words.High_to_Low)
			{
				RELAY=0;
				speed_to_mA_num = &(eeprom_config_words.speed_to_mA_low_num);
				speed_to_mA_den = &(eeprom_config_words.speed_to_mA_low_den);
			}
			Calc_reg=((Calc_reg*(*speed_to_mA_num))/(*speed_to_mA_den));
			mA_out(Calc_reg);//Send mA value to DAC
			//Reset Timer count register
			flags.flags_bits.update_Freq=0;
			TMR1_overflow_cnt=0;
			TMR1H=0;
			TMR1L=0;
			T1GGO=1;//Set single pulse acquisition unit
			TMR1ON=1;//Restart timer 1
		}
        if(!NMEA_cnt_to_send)//approx 1sec repetition rate
        {
			while(UART_TX_buff_index);//Wait in case that buffer is not fully transmitted.
			NMEA_build(UART_TX_buff,Wind_speed,dir_val*25);//25 is scale from segments to degrees (2.5°)
			UART_Write_Text_buffered();
			//UART_Write_Text(UART_TX_buff);
			NMEA_cnt_to_send=NMEA_cnt_to_send_init;
        }
    }
}
