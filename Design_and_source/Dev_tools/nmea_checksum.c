#include <stdio.h>
#include <stdlib.h>

void NMEA_build(char *out,unsigned short speed, unsigned short direction);

int main()
{
    unsigned short speed, direction;
    char temp[50];
    printf("\nGive speed:>");
    scanf("%hu",&speed);
    printf("Give Direction:>");
    scanf("%hu",&direction);
    NMEA_build(temp,speed,direction);
    printf("NMEA string=%s",temp);
    return 0;
}

unsigned char app_strcpy(char *out,char *in)
{
    unsigned char ret=0;
    while(in[ret]!='\0')
    {
	    out[ret]=in[ret];
	    ret++;
    }
    return ret;
}

void checksumtostr(char *out,unsigned char input)
{
    unsigned char i;
    out[0]=(input>>4)&0x0F;
    out[1]=input&0x0F;
    for(i=0;i<2;i++)
    {
	    if(out[i]<10)
	        out[i]+='0';
	    else
	    {
	        out[i]+='A'-10;
	    }
    }
    out[i]='\0';
}

void bin_to_NMEA_val(char *out,unsigned short val)
{
    unsigned char c=0;
    printf("%d\n",val);
    out[5]='\0';
    for(c=4;c!=0xff;c--)
    {
	    if(c==3)
	    {
	        out[c]='.';
	    }
	    else
	    {
	        out[c]=val%10+'0';
	        val/=10;
	    }
    }
}

unsigned char NMEA_checksum_add_vars(const char *val)
{
    unsigned char c=0,i=0;
    while(val[i])
    {
        c ^= val[i];
        i++;
    }
    return c;
}

void NMEA_build(char *out,unsigned short speed, unsigned short direction)
{
    unsigned char checksum = 0x48,i;
    char val[2][6],str_checksum[3];
    bin_to_NMEA_val(val[0],speed);
    bin_to_NMEA_val(val[1],direction);
    checksum^=NMEA_checksum_add_vars(val[0])^NMEA_checksum_add_vars(val[1]);
    checksumtostr(str_checksum,checksum);
    printf("checksum=0x%2x__%s\n",checksum,str_checksum);
    //copy the first 6bytes of the NMEA message "$WIMWV,"
    i=app_strcpy(out,"$WIMWV,");
    //copy speed value string
    i+=app_strcpy((out+i),val[0]);
    //copy the first 3 next bytes of the NMEA message ",T,"
    i+=app_strcpy((out+i),",T,");
    //copy direction value string
    i+=app_strcpy((out+i),val[1]);
    //copy the first 3 next bytes of the NMEA message ",N*"
    i+=app_strcpy((out+i),",N*");
    //copy direction value string
    i+=app_strcpy((out+i),str_checksum);
    //add the \r\n
    app_strcpy((out+i),"\r\n");
    //sprintf(out,"$WIMWV,xx.x,T,xx.x,N*xx\r\n",speed,direction,checksum);
    return;
}

/*unsigned char NMEA_checksum_add_vars(float val)
{
    unsigned char c = 0,i=0;
    char s[5];
    sprintf(s,"%2.1F",val);
    while(s[i])
    {
        c ^= s[i];
        i++;
    }
    return c;
}

void NMEA_build(char *out,float speed, float direction)
{
    unsigned char checksum = 0x48;
    checksum^=NMEA_checksum_add_vars(speed)^NMEA_checksum_add_vars(direction);
    sprintf(out,"$WIMWV,%2.1F,T,%2.1F,N*%02X\r\n",speed,direction,checksum);
    return;
}*/
