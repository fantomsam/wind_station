/*
Compile with:
	gcc par_gray.c -o par_gray -lreadline
*/
#include <fcntl.h>
#include <unistd.h>
#include <linux/parport.h>
#include <linux/ppdev.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <readline/readline.h>
#include <readline/history.h>

void bit_decoder(unsigned char input,char *output);
unsigned char gray_to_bin(unsigned char input);
unsigned char bin_to_gray(unsigned char input);

int main(int argc, char *argv[])
{
    unsigned char val,val_edit,sw=0;
    char temp[100], *buff;
	int	PortFD=0;
	int WR=0;

	if( argc != 3 )
    {
        printf("Usage: %s dev/parport# --bin2gray (or --graytobin)\n", argv[0]);
        return 1;
    }
    else
    {
        if(strstr(argv[2],"--bin2gray"))
            sw=0;
        else if(strstr(argv[2],"--gray2bin"))
            sw=1;
        else
        {
            printf("Argument error\n");
            return 1;
        }
    }

	PortFD = open(argv[1], O_RDWR);
	if(PortFD==-1)
	{
		printf("opening error\n");
		exit(1);
	}
	if(ioctl(PortFD, PPCLAIM))
	{
  	     perror("PPCLAIM");
  	     close(PortFD);
  	     return 10;
 	}
	ioctl(PortFD, PPDATADIR ,&WR);
    while((buff = readline("\n\nGive number\n>")))
    {
        if(strlen(buff))
			add_history(buff);
        sscanf(buff, "%hhu", &val);
		if(strstr(temp,"-1"))
		{
			val=0;
			while(1)
			{
				val_edit=bin_to_gray(val);
				ioctl(PortFD, PPWDATA, &val_edit);
				val++;
				if(val>143)
				{
					val=0;
					WR^=0x01;
					ioctl(PortFD, PPWCONTROL, &WR);
				}
				usleep(10000);
			}
		}
		else
		{
			val=atoi(temp);
			bit_decoder(val,temp);
			printf("Given Number is %sB (0x%02x)\n",temp,val);
			switch(sw)
			{
				case 0: val_edit=bin_to_gray(val);
						bit_decoder(val_edit,temp);
						printf("bin_to_gray(%d)=%d \"%s\" (0x%02x)\n",val,val_edit,temp,val_edit);
						break;
				case 1: val_edit=gray_to_bin(val);
						bit_decoder(val_edit,temp);
						printf("gray_to_bin(%d)=%d \"%s\" (0x%02x)\n",val,val_edit,temp,val_edit);
						break;

			}
			printf("Write data ta parallel\n");
			ioctl(PortFD, PPWDATA, &val_edit);
		}
		free(buff);
    }
    close(PortFD);
    printf("EXIT\n");
	return 0;
}

void bit_decoder(unsigned char input,char *output)
{
    char i;
    for(i=7;i>=0;i--)
    {
        output[7-i]=(input>>i&1)+'0';
    }
    output[8]='\0';
    return;
}

unsigned char bin_to_gray(unsigned char input)
{
    if(input>=72)
        input=input+(255-143);
    return input ^ (input >> 1);
}

unsigned char gray_to_bin(unsigned char input)
{
    unsigned char i;
    for(i=4;i;i>>=1)
        input = input ^ (input >> i);
    if(input<72)
        return input;
    else
        return input-(255-143);
}
