/*
Compile with:
	gcc gray_conv.c -o gray_conv -l readline
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <readline/readline.h>
#include <readline/history.h>

void bit_decoder(unsigned char input,char *output);
unsigned char gray_to_bin(unsigned char input);
unsigned char bin_to_gray(unsigned char input);

int main(int argc, char *argv[])
{
    unsigned char val,val_edit,sw=0;
    char temp[100], *buff;

    if( argc != 2 )
    {
        printf("Usage: %s --bin2gray or --graytobin\n", argv[0]);
        return 1;
    }
    else
    {
        if(strstr(argv[1],"--bin2gray"))
            sw=0;
        else if(strstr(argv[1],"--gray2bin"))
            sw=1;
        else
        {
            printf("Argument error\n");
            return 1;
        }
    }
    while((buff = readline("\n\nGive number\n>")))
    {
		if(strlen(buff))
			add_history(buff);
        sscanf(buff, "%hhu", &val);
        bit_decoder(val, temp);
        printf("Given Number is %sB (0x%02x)\n",temp,val);
        switch(sw)
        {
            case 0: val_edit=bin_to_gray(val);
                    bit_decoder(val_edit,temp);
                    printf("bin_to_gray(%d)=%d \"%s\" (0x%02x)\n",val,val_edit,temp,val_edit);
                    break;
            case 1: val_edit=gray_to_bin(val);
                    bit_decoder(val_edit,temp);
                    printf("gray_to_bin(%d)=%d \"%s\" (0x%02x)\n",val,val_edit,temp,val_edit);
                    break;

        }
        free(buff);
    }
    printf("EXIT\n");
    return 0;
}

void bit_decoder(unsigned char input,char *output)
{
    char i;
    for(i=7;i>=0;i--)
    {
        output[7-i]=(input>>i&1)+'0';
    }
    output[8]='\0';
    return;
}

unsigned char bin_to_gray(unsigned char input)
{
    if(input>=72)
        input=input+(255-143);
    return input ^ (input >> 1);
}

unsigned char gray_to_bin(unsigned char input)
{
    unsigned char i;
    for(i=4;i;i>>=1)
        input = input ^ (input >> i);
    if(input<72)
        return input;
    else
        return input-(255-143);
}

